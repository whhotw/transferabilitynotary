package edu.ntust.jersey;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("api")
public class MyApplication extends ResourceConfig
{

	public MyApplication()
	{
		packages("edu.ntust.jersey.module");
		
	}

}
