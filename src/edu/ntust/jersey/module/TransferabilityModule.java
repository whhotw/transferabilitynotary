package edu.ntust.jersey.module;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import edu.ntust.dao.MySQL;
import edu.ntust.notary.Hash;

// for user b
@Path("transferability")
public class TransferabilityModule
{
	PreparedStatement query = null;
	Connection conn = null;
	String returnString = null;
	JSONObject jobject = null;
	ResultSet rs = null;

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String tranferability(@FormParam("username") String username, @FormParam("password") String password,
			@FormParam("androidid") String androidid, @FormParam("app_id") String app_id, @FormParam("p_id") String p_id,
			@FormParam("pre_username") String pre_username, @FormParam("key") String key) throws Exception
	{
//		String genkey = Hash.sha1(app_id + Hash.sha1(p_id));
//		if (key.compareToIgnoreCase(genkey) != 0)
//		{
//			// verify key fail
//			return "0";
//		}

		try
		{
			queryMemberTable(username, password, androidid);

			while (rs.next())
			{
				jobject = new JSONObject();
				if (rs.getInt("count") != 1)
				{
					jobject.put("login", "fail");
				}
				else
				{
					jobject.put("login", "pass");

					updatePurchaseTable(username, p_id);
					insertTransferTable(p_id, app_id, pre_username, username);

					jobject.put("app_uri", key);
				}

				returnString = jobject.toString();
			}

			query.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (conn != null)
				conn.close();
		}

		return returnString;
	}

	private void queryMemberTable(String username, String password, String androidid)
	{
		try
		{
			conn = MySQL.getInstance().getConnection();
			query = conn
					.prepareStatement("select count(*) count from member where member.username = ? and member.password = ? and member.androidid = ?");
			query.setString(1, username);
			query.setString(2, Hash.sha1(password));
			query.setString(3, androidid);

			rs = query.executeQuery();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
	}

	private void updatePurchaseTable(String username, String p_id)
	{
		// Update app purchase record from purchase table

		try
		{
			query = conn.prepareStatement("update purchase set purchase.username = ?, purchase.times=purchase.times+1 where p_id = ?");
			query.setString(1, username);
			query.setString(2, p_id);
			jobject.put("update", query.executeUpdate());
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void insertTransferTable(String p_id, String app_id, String pre_username, String username)
	{
		// Insert app transfer record to transfer table

		try
		{
			query = conn.prepareStatement("INSERT INTO transfer (p_id, app_id, pre_username_id, username_id) VALUES (?, ?, ?, ?);");
			query.setInt(1, Integer.valueOf(p_id));
			query.setString(2, app_id);
			query.setString(3, pre_username);
			query.setString(4, username);
			jobject.put("insert", query.executeUpdate());
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
}
