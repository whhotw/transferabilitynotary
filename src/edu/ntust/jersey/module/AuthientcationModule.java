package edu.ntust.jersey.module;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.internal.util.Base64;
import org.json.JSONObject;

import edu.ntust.dao.MySQL;
import edu.ntust.notary.Global;
import edu.ntust.notary.Hash;

@Path("auth")
public class AuthientcationModule
{
	private String recuresiveHash(int times, String source) throws NoSuchAlgorithmException
	{
		if (times == 0)
		{
			return source;
		}
		else
		{
			return Hash.sha1(recuresiveHash(times - 1, source) + Global.SECURE_VALUE);
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String authientcation(@FormParam("username") String username, @FormParam("password") String password,
			@FormParam("androidid") String androidid, @FormParam("app_id") String app_id, @FormParam("key") String key) throws Exception
	{

		PreparedStatement query = null;
		Connection conn = null;
		String returnString = null;
		try
		{
			conn = MySQL.getInstance().getConnection();
			query = conn
					.prepareStatement("select count(purchase.app_id) count, purchase.p_id, times, total_transfer_times from member inner join purchase on member.username = purchase.username where member.username = ? and member.password = ? and member.androidid = ? and purchase.app_id = ?");
			query.setString(1, username);
			query.setString(2, Hash.sha1(password));
			query.setString(3, androidid);
			query.setString(4, app_id);
			ResultSet rs = query.executeQuery();
			while (rs.next())
			{
				JSONObject jobject = new JSONObject();
				if (rs.getInt("count") != 1)
				{
					jobject.put("login", "fail_from_server_1");
				}
				else
				{
					
					jobject.put("login", "pass");
					jobject.put("p_id", String.valueOf(rs.getInt("p_id")));

					// if have 'key' so that transfer again process
					String pidsha1 = Hash.sha1(String.valueOf(rs.getInt("p_id")));
					if (key.length() != 0)
					{
						jobject.put("dbg", "dbg_1");
						String source = app_id + pidsha1;
						String tmp_hash_value = recuresiveHash(rs.getInt("times")+1, source);
						String recursive_hash_value = recuresiveHash(rs.getInt("total_transfer_times"), source);
						jobject.put("recursive_hash_value", recursive_hash_value);
						
						if (tmp_hash_value.compareTo(recursive_hash_value) != 0)
						{
							jobject.put("key", tmp_hash_value);
						}
						else
						{
							jobject.put("key", tmp_hash_value);
							jobject.put("login", "fail_from_server_2");
							jobject.put("err_msg", "transfer denied");
						}
					}
					else
					{
						jobject.put("dbg", "dbg_2");
						jobject.put("dbg_a", app_id);
						jobject.put("dbg_b", pidsha1);
						jobject.put("dbg_c", Global.SECURE_VALUE);
						jobject.put("key", Hash.sha1(app_id + pidsha1 + Global.SECURE_VALUE));
					}

					jobject.put("username", username);
					jobject.put("app_id", app_id);
				}

				returnString = jobject.toString();
			}

			query.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (conn != null)
				conn.close();
		}

		return returnString;
	}

}
