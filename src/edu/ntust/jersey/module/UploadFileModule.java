package edu.ntust.jersey.module;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONException;
import org.json.JSONObject;

import edu.ntust.notary.Hash;

// for upload apk
@Path("upload")
public class UploadFileModule
{

	@Context
	ServletContext context;

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String uploadFile(@Context HttpServletRequest request)
	{
		String uploadedFileLocation = context.getRealPath("/apps");
		JSONObject jobject = new JSONObject();

		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart)
		{

		}
		else
		{
			try
			{
				jobject.put("upload", "0");

				DiskFileItemFactory factory = new DiskFileItemFactory();
				// maximum size that will be stored in memory
				factory.setSizeThreshold(4 * 1024 * 1024);
				// Location to save data that is larger than maxMemSize.
				factory.setRepository(new File(context.getRealPath("/temp")));

				// Create a new file upload handler
				ServletFileUpload upload = new ServletFileUpload(factory);
				// maximum file size to be uploaded.
				upload.setSizeMax(10 * 1024 * 1024);

				// Parse the request to get file items.
				List<FileItem> fileItems = upload.parseRequest(request);

				// Process the uploaded file items
				Iterator<FileItem> i = (Iterator<FileItem>) fileItems.iterator();

				while (i.hasNext())
				{
					FileItem fi = (FileItem) i.next();
					if (!fi.isFormField())
					{
						// Get the uploaded file parameters
						String fileName = fi.getName();

						// Write the file
						File file = new File(uploadedFileLocation, fileName);
						fi.write(file);
					}
				}

				jobject.put("upload", "success");
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}

		return jobject.toString();

	}
}
