package edu.ntust.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import edu.ntust.notary.Global;

public class MySQL
{

	private static MySQL mysql = null;

	public MySQL() throws ClassNotFoundException
	{
		Class.forName("com.mysql.jdbc.Driver");
	}

	public static MySQL getInstance()
	{
		if (mysql != null)
		{
			return mysql;
		}
		
		try
		{
			if (mysql == null)
			{
				mysql = new MySQL();
			}
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		
		return mysql;
	}

	public Connection getConnection() throws SQLException
	{
		return DriverManager.getConnection("jdbc:mysql://" + Global.SERVER_URL + ":3306/" + Global.DB_NAME, Global.USER, Global.PASS);
	}
}
//
//	public void updateDataBase(String sql)
//	{
//		try
//		{
//			setPreparedStatement(connect.prepareStatement(sql));
//		}
//		catch (SQLException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	public PreparedStatement getPreparedStatement()
//	{
//		return preparedStatement;
//	}
//
//	public void setPreparedStatement(PreparedStatement preparedStatement)
//	{
//		this.preparedStatement = preparedStatement;
//	}
//
//	public int getUpdateResuult() throws SQLException
//	{
//		return preparedStatement.executeUpdate();
//	}
//
//	public void setUpdateResuult(int updateResuult)
//	{
//		this.updateResuult = updateResuult;
//	}
//
//	public void readDataBase(String sql, String[] prams) throws Exception
//	{
//		try
//		{
//
//			// This will load the MySQL driver, each DB has its own driver
//			// MainServlet.out.println("1");
//			Class.forName("com.mysql.jdbc.Driver");
//			// Setup the connection with the DB
//			// MainServlet.out.println("2");
//			connect = DriverManager.getConnection("jdbc:mysql://" + serverURL + ":3306/" + db, USER, PASS);
//			// AuthServlet.out.println(sql);
//			// Statements allow to issue SQL queries to the database
//			// statement = connect.createStatement();
//			// Result set get the result of the SQL query
//			// resultSet = statement.executeQuery(sql);
//			// writeResultSet(resultSet);
//
//			// PreparedStatements can use variables and are more efficient
//			preparedStatement = connect.prepareStatement(sql);
//			// "myuser, webpage, datum, summary, COMMENTS from FEEDBACK.COMMENTS");
//			// Parameters start with 1
//
//			for (int i = 0; i < prams.length; i++)
//			{
//				// AuthServlet.out.println(prams[i]);
//				preparedStatement.setString(i + 1, prams[i]);
//			}
//			setResultSet(preparedStatement.executeQuery());
//
//			// preparedStatement =
//			// connect.prepareStatement("SELECT myuser, webpage, datum, summary, COMMENTS from FEEDBACK.COMMENTS");
//			// resultSet = preparedStatement.executeQuery();
//			// writeResultSet(resultSet);
//
//			// Remove again the insert comment
//			// preparedStatement =
//			// connect.prepareStatement("delete from FEEDBACK.COMMENTS where myuser= ? ; ");
//			// preparedStatement.setString(1, "Test");
//			// preparedStatement.executeUpdate();
//
//			// resultSet =
//			// statement.executeQuery("select * from FEEDBACK.COMMENTS");
//			// writeMetaData(resultSet);
//
//		}
//		catch (SQLException e)
//		{
//			AuthServlet.out.println(e);
//		}
//		finally
//		{
//			// close();
//		}
//
//	}
//
//	public String getServerURL()
//	{
//		return serverURL;
//	}
//
//	public void setServerURL(String serverURL)
//	{
//		this.serverURL = serverURL;
//	}
//
//	public String getDb()
//	{
//		return db;
//	}
//
//	public void setDb(String db)
//	{
//		this.db = db;
//	}
//
//	public String getUSER()
//	{
//		return USER;
//	}
//
//	public void setUSER(String uSER)
//	{
//		USER = uSER;
//	}
//
//	public String getPASS()
//	{
//		return PASS;
//	}
//
//	public void setPASS(String pASS)
//	{
//		PASS = pASS;
//	}
//
//	public ResultSet getResultSet()
//	{
//		return resultSet;
//	}
//
//	public void setResultSet(ResultSet resultSet)
//	{
//		this.resultSet = resultSet;
//	}
//
//	private void writeMetaData(ResultSet resultSet) throws SQLException
//	{
//		// Now get some metadata from the database
//		// Result set get the result of the SQL query
//
//		System.out.println("The columns in the table are: ");
//
//		System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
//		for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++)
//		{
//			System.out.println("Column " + i + " " + resultSet.getMetaData().getColumnName(i));
//		}
//	}
//
//	private void writeResultSet(ResultSet resultSet) throws SQLException
//	{
//		// ResultSet is initially before the first data set
//		while (resultSet.next())
//		{
//			// It is possible to get the columns via name
//			// also possible to get the columns via the column number
//			// which starts at 1
//			// e.g. resultSet.getSTring(2);
//			String username = resultSet.getString("username");
//			String password = resultSet.getString("password");
//		}
//	}
//
//	// You need to close the resultSet
//	public void close()
//	{
//		try
//		{
//			if (resultSet != null)
//			{
//				resultSet.close();
//			}
//
//			if (statement != null)
//			{
//				statement.close();
//			}
//
//			if (connect != null)
//			{
//				connect.close();
//			}
//		}
//		catch (Exception e)
//		{
//
//		}
//	}
//
//}
