**[Update Log]**

1. 在purchase表格中新增兩個欄位，分別是[times], [total_transfer_times]

2. [times] 表示轉手次數

3. [total_transfer_times] 總共可以轉移次數。而剩餘轉移的次數為[total_transfer_times] - [times] - 1(第一次購買)

4. 假如是因為達轉移上限則會顯示transfer denied訊息
![Screen Shot 2014-08-26 at 12.49.17 PM.png](https://bitbucket.org/repo/yAXBez/images/3821994071-Screen%20Shot%202014-08-26%20at%2012.49.17%20PM.png)
![Screenshot_2014-08-26-12-58-06.png](https://bitbucket.org/repo/yAXBez/images/3135328117-Screenshot_2014-08-26-12-58-06.png)